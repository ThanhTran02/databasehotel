/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

CREATE TABLE `Booking` (
  `BookingID` int NOT NULL AUTO_INCREMENT,
  `RoomID` int DEFAULT NULL,
  `CheckinDate` date DEFAULT NULL,
  `CheckoutDate` date DEFAULT NULL,
  `TotalPrice` decimal(10,2) DEFAULT NULL,
  `IsPaid` tinyint(1) DEFAULT '0',
  `GuestID` int DEFAULT NULL,
  PRIMARY KEY (`BookingID`),
  KEY `fk_room_id` (`RoomID`),
  KEY `fk_guest_id` (`GuestID`),
  CONSTRAINT `fk_guest_id` FOREIGN KEY (`GuestID`) REFERENCES `Guest` (`GuestID`),
  CONSTRAINT `fk_room_id` FOREIGN KEY (`RoomID`) REFERENCES `Room` (`RoomID`)
) ENGINE=InnoDB AUTO_INCREMENT=80 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `BookingGuest` (
  `BookingID` int NOT NULL,
  `GuestID` int NOT NULL,
  PRIMARY KEY (`BookingID`,`GuestID`),
  KEY `GuestID` (`GuestID`),
  CONSTRAINT `BookingGuest_ibfk_1` FOREIGN KEY (`BookingID`) REFERENCES `Booking` (`BookingID`),
  CONSTRAINT `BookingGuest_ibfk_2` FOREIGN KEY (`GuestID`) REFERENCES `Guest` (`GuestID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `Guest` (
  `GuestID` int NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) DEFAULT NULL,
  `Address` varchar(255) DEFAULT NULL,
  `Phone` varchar(15) DEFAULT NULL,
  `Cmnd` varchar(15) DEFAULT NULL,
  `TypeGuestID` int NOT NULL DEFAULT '1',
  `Email` varchar(255) DEFAULT NULL,
  `Password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `IsDeleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`GuestID`),
  KEY `TypeGuestID` (`TypeGuestID`),
  CONSTRAINT `Guest_ibfk_1` FOREIGN KEY (`TypeGuestID`) REFERENCES `GuestType` (`GuestTypeID`)
) ENGINE=InnoDB AUTO_INCREMENT=107 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `GuestType` (
  `GuestTypeID` int NOT NULL AUTO_INCREMENT,
  `Description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`GuestTypeID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `Regulation` (
  `RegulationID` int NOT NULL,
  `Description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT 'NULL',
  `Value` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  PRIMARY KEY (`RegulationID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `Room` (
  `RoomID` int NOT NULL,
  `TypeRoomID` int DEFAULT NULL,
  `Status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`RoomID`),
  KEY `Room_ibfk_1` (`TypeRoomID`),
  CONSTRAINT `Room_ibfk_1` FOREIGN KEY (`TypeRoomID`) REFERENCES `TypeRoom` (`TypeRoomID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `Staff` (
  `StaffID` int NOT NULL AUTO_INCREMENT,
  `Email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `Password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `Name` varchar(50) DEFAULT NULL,
  `Position` varchar(50) DEFAULT NULL,
  `DateOfBirth` date DEFAULT NULL,
  `Phone` varchar(15) DEFAULT NULL,
  `IsDeleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`StaffID`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `TypeRoom` (
  `TypeRoomID` int NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `PricePerDay` decimal(10,2) DEFAULT NULL,
  `MaxGuests` int DEFAULT '3',
  PRIMARY KEY (`TypeRoomID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `Booking` (`BookingID`, `RoomID`, `CheckinDate`, `CheckoutDate`, `TotalPrice`, `IsPaid`, `GuestID`) VALUES
(43, 102, '2024-05-03', '2024-05-04', '225000.00', 1, NULL);
INSERT INTO `Booking` (`BookingID`, `RoomID`, `CheckinDate`, `CheckoutDate`, `TotalPrice`, `IsPaid`, `GuestID`) VALUES
(59, 101, '2023-01-31', '2023-02-03', '450000.00', 1, NULL);
INSERT INTO `Booking` (`BookingID`, `RoomID`, `CheckinDate`, `CheckoutDate`, `TotalPrice`, `IsPaid`, `GuestID`) VALUES
(60, 202, '2023-02-02', '2023-02-03', '255000.00', 1, NULL);
INSERT INTO `Booking` (`BookingID`, `RoomID`, `CheckinDate`, `CheckoutDate`, `TotalPrice`, `IsPaid`, `GuestID`) VALUES
(61, 301, '2023-09-15', '2023-09-19', '800000.00', 1, NULL),
(62, 202, '2024-01-31', '2024-02-02', '340000.00', 1, NULL),
(63, 302, '2024-02-29', '2024-03-04', '1000000.00', 1, NULL),
(64, 202, '2024-05-27', '2024-05-28', '170000.00', 1, NULL),
(65, 101, '2024-05-27', '2024-05-31', '600000.00', 0, NULL),
(66, 303, '2023-05-10', '2023-05-15', '1500000.00', 1, NULL),
(67, 302, '2023-10-24', '2023-10-27', '600000.00', 1, NULL),
(68, 302, '2023-12-09', '2023-12-12', '600000.00', 1, NULL),
(69, 102, '2024-03-07', '2024-03-08', '150000.00', 1, 31),
(70, 203, '2023-08-15', '2023-08-17', '340000.00', 1, NULL),
(71, 102, '2023-09-12', '2023-09-14', '300000.00', 1, NULL),
(72, 102, '2023-08-13', '2023-08-14', '150000.00', 1, NULL),
(73, 201, '2024-05-27', '2024-05-29', '340000.00', 1, NULL),
(74, 101, '2024-05-12', '2024-05-15', '480000.00', 0, 31),
(75, 102, '2024-05-10', '2024-05-11', '160000.00', 0, NULL),
(77, 203, '2024-05-28', '2024-05-29', '170000.00', 0, 103),
(78, 402, '2024-05-28', '2024-05-30', '400000.00', 1, NULL),
(79, 202, '2024-06-06', '2024-06-07', '170000.00', 0, NULL);

INSERT INTO `BookingGuest` (`BookingID`, `GuestID`) VALUES
(43, 58);
INSERT INTO `BookingGuest` (`BookingID`, `GuestID`) VALUES
(59, 79);
INSERT INTO `BookingGuest` (`BookingID`, `GuestID`) VALUES
(60, 80);
INSERT INTO `BookingGuest` (`BookingID`, `GuestID`) VALUES
(60, 81),
(61, 82),
(62, 83),
(63, 84),
(63, 85),
(63, 86),
(64, 87),
(64, 88),
(65, 89),
(66, 90),
(67, 91),
(68, 92),
(69, 93),
(70, 94),
(71, 96),
(72, 97),
(73, 98),
(74, 99),
(75, 100),
(77, 104),
(78, 105),
(79, 106);

INSERT INTO `Guest` (`GuestID`, `Name`, `Address`, `Phone`, `Cmnd`, `TypeGuestID`, `Email`, `Password`, `IsDeleted`) VALUES
(1, 'Bế Trương Dương', 'Hồ Chí Minh', '0912345678', '123456789', 1, '1@gmail.com', '1', 1);
INSERT INTO `Guest` (`GuestID`, `Name`, `Address`, `Phone`, `Cmnd`, `TypeGuestID`, `Email`, `Password`, `IsDeleted`) VALUES
(2, 'John Doe', 'Hồ Chí Minh', '987654321', '987654321', 2, '2@gmail.com', '2', 1);
INSERT INTO `Guest` (`GuestID`, `Name`, `Address`, `Phone`, `Cmnd`, `TypeGuestID`, `Email`, `Password`, `IsDeleted`) VALUES
(3, 'Nguyễn Văn Sĩ', 'Hà Nội', '04567890120', '4567890121', 1, 'si@gmail.com', '3', 0);
INSERT INTO `Guest` (`GuestID`, `Name`, `Address`, `Phone`, `Cmnd`, `TypeGuestID`, `Email`, `Password`, `IsDeleted`) VALUES
(4, 'Hoàng Thị Thu Thủy', 'Hồ Chí Minh', '6543219871', '0978901234', 2, '4@gmail.com', '4', 0),
(5, 'Trần Thị An', 'Hà Nội', '0123456789', '11111111', 1, '5@gmail.com', '5', 0),
(6, 'Lê Văn Bình', 'Đà Nẵng', '0234567890', '22222222', 1, '6@gmail.com', '6', 0),
(7, 'Phạm Thị Cúc', 'Hồ Chí Minh', '0345678901', '333333333', 1, 'ptc@gmail.com', '7', 0),
(8, 'Đỗ Minh Dũng', 'Hải Phòng', '0456789012', '444444444', 1, '8@gmail.com', '8', 0),
(9, 'Nguyễn Thị Thanh', 'Hà Nội', '0567890123', '555555555', 2, '9@gmail.com', '9', 0),
(10, 'Vũ Văn Thắng', 'Hồ Chí Minh', '0678901234', '666666666', 1, '10@gmail.com', '10', 0),
(11, 'Trần Thị Hương', 'Đà Nẵng', '0789012345', '777777777', 2, '11@gmail.com', '11', 0),
(21, 'John Doe', '123 Main St, City', '123-456-7890', '123456789', 2, NULL, NULL, 0),
(22, 'Veronica Klein', 'Voluptatem delectus', NULL, '123456789', 1, NULL, NULL, 0),
(23, 'Bế', 'Sài gòn', NULL, '123456789', 1, NULL, NULL, 0),
(24, 'Dương', 'Sài gòn', NULL, '123456789', 2, NULL, NULL, 0),
(31, 'Nguyễn Văn D', 'Sài gòn', NULL, '123456789', 1, '1111@gmail.com', '$2b$10$iVNQC077kpfRWcCDTOsBYuFwzH0JSc.tpviHPb8X7nwkQeii.mAda', 0),
(32, 'Nguyễn Văn D', 'Sài gòn', NULL, '123456789', 1, NULL, NULL, 0),
(33, 'Nguyễn Văn c', 'Sài gòn', NULL, '123456789', 1, NULL, NULL, 0),
(34, 'Nguyễn Văn B', 'Sài gòn', NULL, '123456789', 1, NULL, NULL, 0),
(35, 'Nguyễn Văn A', 'Sài gòn', NULL, '123456789', 2, NULL, NULL, 0),
(36, 'Nguyễn Văn AB', 'Sài gòn', NULL, '123456789', 1, NULL, NULL, 0),
(41, 'Nguyễn Văn Sĩ', 'Sài gòn', NULL, '123456', 1, NULL, NULL, 0),
(58, 'Sai', 'Sài gòn', NULL, '12345687', 1, NULL, NULL, 0),
(59, 'Mạnh', 'Sài gòn', NULL, '123456', 1, NULL, NULL, 0),
(77, 'Hoàng Mạnh', 'Hà Nội', NULL, '12345678', 1, NULL, NULL, 0),
(78, 'Phạm Thị Cúc', 'Sài gòn', NULL, '12345687', 2, NULL, NULL, 0),
(79, 'Hoàng Thị Thu Thủy', 'Sài gòn', NULL, '123456789', 1, NULL, NULL, 0),
(80, 'A', 'Sài gòn', NULL, '12345678', 2, NULL, NULL, 0),
(81, 'B', 'Hà Nội', NULL, '12345678', 1, NULL, NULL, 0),
(82, 'Nguyễn Văn Sĩ', 'Sài gòn', NULL, '12345678', 1, NULL, NULL, 0),
(83, 'Hoàng Mạnh', 'Sài gòn', NULL, '12345678', 1, NULL, NULL, 0),
(84, 'Trần Thị Hương', 'Sài gòn', NULL, '12345678', 1, NULL, NULL, 0),
(85, 'Kim Dung', 'Vũng Tàu', NULL, '12345678', 1, NULL, NULL, 0),
(86, 'Nguyễn Tuấn', 'Sài gòn', NULL, '12345687', 1, NULL, NULL, 0),
(87, 'Ngô Hưu Nguyên', 'Sài gòn', NULL, '12345678', 1, NULL, NULL, 0),
(88, 'Hoàng Mạnh', 'Vũng Tàu', NULL, '12345678', 1, NULL, NULL, 0),
(89, 'Nguyễn Tuấn Anh', 'Đắk Nông', NULL, '12345678', 1, NULL, NULL, 0),
(90, 'Jon', 'Sài gòn', NULL, '12345678', 2, NULL, NULL, 0),
(91, 'Tưởng Minh Nhuận', 'Đắk Nông', NULL, '12345678', 1, NULL, NULL, 0),
(92, 'Hoàng Thị Thu Thủy', 'Đắk Nông', NULL, '12345678', 1, NULL, NULL, 0),
(93, 'Huyễn Hương', NULL, NULL, '123456789', 1, NULL, NULL, 0),
(94, 'Nguyễn Tuấn Anh', 'Đắk Nông', NULL, '12345678', 1, NULL, NULL, 0),
(95, NULL, NULL, NULL, NULL, 1, 'Be@gmail.com', '$2b$10$ddqE50s4LR7DOE1WqlTaEetlF0YJUG7Imjj04fakw7uINAKnZ8DtK', 0),
(96, 'Nguyễn Tuấn Anh', 'Đắk Nông', NULL, '12345678', 1, NULL, NULL, 0),
(97, 'Nguyễn Văn Sĩ', 'Đắk Nông', NULL, '12345678', 1, NULL, NULL, 0),
(98, 'Nguyễn Văn Sĩ', 'Đắk Nông', NULL, '12345678', 1, NULL, NULL, 0),
(99, 'Hoang Duonh', NULL, NULL, '12345678', 1, NULL, NULL, 0),
(100, 'Nguyễn Văn Sĩ', 'Đắk Nông', NULL, '12345678', 1, NULL, NULL, 0),
(103, NULL, NULL, NULL, NULL, 1, 'thanhcuanam02@gmail.com', '$2b$10$KwY1At/qaINubWi3rButreHu1DzTggqZQJZt8NKrvcEgyqvJtC.9i', 0),
(104, 'Thành', NULL, NULL, '12345678', 1, NULL, NULL, 0),
(105, 'Nguyễn Văn Sĩ', 'Đắk Nông', NULL, '12345678', 1, NULL, NULL, 0),
(106, 'Nguyễn Tuấn Anh', 'Đắk Nông', NULL, '12345678', 1, NULL, NULL, 0);

INSERT INTO `GuestType` (`GuestTypeID`, `Description`) VALUES
(1, 'Nội địa');
INSERT INTO `GuestType` (`GuestTypeID`, `Description`) VALUES
(2, 'Nước ngoài');


INSERT INTO `Regulation` (`RegulationID`, `Description`, `Value`) VALUES
(1, 'Phụ phí khách hàng thứ 3', '0.25');
INSERT INTO `Regulation` (`RegulationID`, `Description`, `Value`) VALUES
(2, 'Phụ phí khách người ngoài', '1.5');


INSERT INTO `Room` (`RoomID`, `TypeRoomID`, `Status`) VALUES
(101, 1, 0);
INSERT INTO `Room` (`RoomID`, `TypeRoomID`, `Status`) VALUES
(102, 1, 0);
INSERT INTO `Room` (`RoomID`, `TypeRoomID`, `Status`) VALUES
(103, 1, 0);
INSERT INTO `Room` (`RoomID`, `TypeRoomID`, `Status`) VALUES
(201, 2, 1),
(202, 2, 0),
(203, 2, 0),
(301, 3, 1),
(302, 3, 1),
(303, 3, 1),
(401, 1, 1),
(402, 3, 1);

INSERT INTO `Staff` (`StaffID`, `Email`, `Password`, `Name`, `Position`, `DateOfBirth`, `Phone`, `IsDeleted`) VALUES
(1, 'nva@gmail.com', '123456', 'Nguyễn Văn A', 'Nhân viên lễ tân', '1990-05-15', '0987654320', 0);
INSERT INTO `Staff` (`StaffID`, `Email`, `Password`, `Name`, `Position`, `DateOfBirth`, `Phone`, `IsDeleted`) VALUES
(2, 'ttb@example.com', '1', 'Trần Thị B', 'Nhân viên lễ tân', '1985-10-20', '0912345678', 0);
INSERT INTO `Staff` (`StaffID`, `Email`, `Password`, `Name`, `Position`, `DateOfBirth`, `Phone`, `IsDeleted`) VALUES
(3, 'pdc@example.com', '1', 'Phạm Đình Cường', 'Nhân viên phục vụ', '1995-03-25', '0965432109', 0);
INSERT INTO `Staff` (`StaffID`, `Email`, `Password`, `Name`, `Position`, `DateOfBirth`, `Phone`, `IsDeleted`) VALUES
(4, 'ltd@example.com', '1', 'Lê Thị Dương', 'Kế toán', '1988-07-12', '0943210987', 0),
(5, 'hve@example.com', '1', 'Hoàng Văn Em', 'Bảo vệ', '1983-12-08', '0908765432', 0),
(6, 'A@gmail.com', '1', 'Nguyen Van A', 'Lễ Tân', '1985-10-20', '033333333', 0),
(7, 'wupoleh@mailinator.com', '1', 'Herrod Harvey', 'Nhân viên phục vụ', '1994-04-22', '0123566611', 0);

INSERT INTO `TypeRoom` (`TypeRoomID`, `Name`, `Description`, `PricePerDay`, `MaxGuests`) VALUES
(1, 'Phòng bình dân', 'Phòng tiêu chuẩn', '160000.00', 3);
INSERT INTO `TypeRoom` (`TypeRoomID`, `Name`, `Description`, `PricePerDay`, `MaxGuests`) VALUES
(2, 'Phòng cận cao cấp', 'Phòng đây đủ tiện nghi', '170000.00', 3);
INSERT INTO `TypeRoom` (`TypeRoomID`, `Name`, `Description`, `PricePerDay`, `MaxGuests`) VALUES
(3, 'Phòng cao cấp', 'Phòng đây đủ tiện nghi', '200000.00', 4);


/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;